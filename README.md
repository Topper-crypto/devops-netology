# devops-netology Korovin
конфиг гитигнор исключает файлы с:

devops-netology
**/.terraform/*
локальные папки терраформ

.tfstate files
файлы состояний

crash.log
логи крашей

*.tfvars
файлы с переменными, паролями, ключами и др чувствительные данные

override.tf
override.tf.json
*_override.tf
*_override.tf.json

файлы переопределений 

!example_override.tf
файлы переопределений с отрицанием

example: *tfplan*
файлы для игнорирования вывода плана команды: terraform plan -out = tfplan

.terraformrc
terraform.rc
Файлы конфигурации CLI
